## 介紹
這個模組主要是Drupal Commerce的付款方式插件，整合簡單付（ezPay）裡面的支付寶付款方式，由於這個方法主要是台灣才適用，所以全部的內容都是以繁體中文來撰寫

* 如果想要看到完整的描述，可以到以下連結
   https://www.drupal.org/project/commerce_ezpay

* 如果有問題需要解決，或是錯誤回報、建議，請到下方連結
   https://www.drupal.org/project/issues/commerce_ezpay



## 安裝方法
 
* 正常的安裝方式安裝，如果您不熟悉可以參考以下官網的安裝教學
   https://www.drupal.org/documentation/install/modules-themes/modules-7


## 設定
 
 * 設定畫面 admin/commerce/config/ezpay


## 維護

### 目前維護人員:
 * Victor Yang (cobenash) - https://www.drupal.org/user/2138470


### 這個專案由以下公司所贊助
 * Hello Santa Corp.
   We specialize in Drupal website development and provide module development 
   and web server services.Visit https://www.hellosanta.com.tw/ for further information.