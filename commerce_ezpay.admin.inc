<?php

/**
 * @file commerce_ezpay.admin.inc
 * 
 */

function commerce_ezpay_settings_form($form, &$form_state)
{
    $form['ezpayStatus'] = array(
        '#type' => 'radios',
        '#title' => '簡單付系統狀態',
        '#description' => '選擇對應的系統狀態。例如：測試環境、正式環境',
        '#options' => array(
            'dev' => '測試環境',
            'production' => '正式環境',
        ),
        '#default_value' => variable_get('ezpayStatus', 'dev'),
        '#required' => TRUE,
        '#weight' => 0,
    );

    $form['ezpayMerchantID'] = array(
        '#type' => 'textfield',
        '#title' => 'MerchantID',
        '#description' => '簡單付MerchantID，需要經過申請才會有此ID，測試環境也需要申請',
        '#maxlength' => 255,
        '#size' => 30,
        '#required' => TRUE,
        '#default_value' => variable_get('ezpayMerchantID', 'PG300000005853'),
        '#weight' => 1,
    );

    $form['ezpayHashKey'] = array(
        '#type' => 'textfield',
        '#title' => 'HashKey',
        '#description' => '簡單付HashKey，需要經過申請才會有此ID，測試環境也需要申請',
        '#maxlength' => 255,
        '#size' => 30,
        '#required' => TRUE,
        '#default_value' => variable_get('ezpayHashKey', 'gAttDrD6Q0COwHVG7YcXf47wRMUlfgCw'),
        '#weight' => 2,
    );

    $form['ezpayHashIV'] = array(
        '#type' => 'textfield',
        '#title' => 'HashIV',
        '#description' => '簡單付HashIV，需要經過申請才會有此ID，測試環境也需要申請',
        '#maxlength' => 255,
        '#size' => 30,
        '#required' => TRUE,
        '#default_value' => variable_get('ezpayHashIV', 'Ckbk8Ik6POLqtjnP'),
        '#weight' => 3,
    );

    $form['ezpayItemDesc'] = array(
        '#type' => 'textfield',
        '#title' => '商品資訊',
        '#description' => '長度50字，請勿使用特殊字元，並且使用Utf-8編碼方式',
        '#maxlength' => 50,
        '#size' => 30,
        '#required' => TRUE,
        '#default_value' => variable_get('ezpayItemDesc', '使用HelloSanta模組購買商品'),
        '#weight' => 4,
    );

    $form['ezpayOrderComment'] = array(
        '#type' => 'textarea',
        '#title' => '商店備註',
        '#description' => '1.限制長度為 300 字。2.若有提供此參數，將會於 MPG 頁面呈現商店備註內容。',
        '#maxlength' => 300,
        '#default_value' => variable_get('ezpayOrderComment'),
        '#weight' => 5,
    );

    $form['ezpayEmailModify'] = array(
        '#type' => 'radios',
        '#title' => '付款人電子信箱是否開放修改',
        '#description' => '設定於 MPG 頁面，付款人電子信箱欄位是否開放讓付款人修改',
        '#options' => array(
            1 => '可修改',
            0 => '不可修改',
        ),
        '#default_value' => variable_get('ezpayEmailModify', 0),
        '#weight' => 6,
    );

    $form['ezpayLoginType'] = array(
        '#type' => 'radios',
        '#title' => '簡單付會員',
        '#description' => '是否需要是會員才能使用金流',
        '#options' => array(
            1 => '須要登入簡單付會員',
            0 => '不須登入簡單付會員',
        ),
        '#default_value' => variable_get('ezpayLoginType', 0),
        '#weight' => 7,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#weight' => 10,
        '#value' => '儲存',
    );
    return $form;
}

function commerce_ezpay_settings_form_submit($form, &$form_state)
{

    $ezpayStatus = $form_state['values']['ezpayStatus'];
    $ezpayMerchantID = $form_state['values']['ezpayMerchantID'];
    $ezpayHashKey = $form_state['values']['ezpayHashKey'];
    $ezpayHashIV = $form_state['values']['ezpayHashIV'];
    $ezpayItemDesc = $form_state['values']['ezpayItemDesc'];
    $ezpayOrderComment = $form_state['values']['ezpayOrderComment'];
    $ezpayEmailModify = $form_state['values']['ezpayEmailModify'];
    $ezpayLoginType = $form_state['values']['ezpayLoginType'];

    variable_set('ezpayStatus', $ezpayStatus);
    variable_set('ezpayMerchantID', $ezpayMerchantID);
    variable_set('ezpayHashKey', $ezpayHashKey);
    variable_set('ezpayHashIV', $ezpayHashIV);
    variable_set('ezpayItemDesc', $ezpayItemDesc);
    variable_set('ezpayOrderComment', $ezpayOrderComment);
    variable_set('ezpayEmailModify', $ezpayEmailModify);
    variable_set('ezpayLoginType', $ezpayLoginType);

    drupal_set_message('修改更新成功');
}
