<?php

/**
 * @file commerce_ezpay_alipay.inc
 * 主要來處理支付寶的相關服務
 */

/**
 * Implements CALLBACK_commerce_payment_method_settings_form
 */

function ezpay_alipay_settings_form($settings = NULL)
{

    $form = array();
    $form['TradeLimit'] = array(
        '#type' => 'textfield',
        '#title' => '交易限制秒數',
        '#description' => '下限為 60 秒，當秒數介於 1~59 秒時，會以 60 秒計算；上限為900秒，超過900秒會以900來記錄',
        '#size' => 30,
        '#default_value' => empty($settings['TradeLimit']) ? '60' : $settings['TradeLimit'],
    );
    $form['CrossMobile'] = array(
        '#type' => 'select',
        '#title' => '啟用銀聯卡',
        '#description' => '預設為不啟用',
        '#options' => array(
            '1' => '使用Wap交易',
            '0' => '使用Web交易'
        ),
        '#default_value' => empty($settings['CrossMobile']) ? '0' : $settings['CrossMobile'],
    );

    return $form;
}

/**
 * Implements CALLBACK_commerce_payment_method_redirect_form
 */
function ezpay_alipay_redirect_form($form, &$form_state, $order, $payment_method)
{

    global $base_url;
    
    //基本參數
    $ActionURL = EZPAY_POST_SERVICE_URL;
    $HashKey = variable_get('ezpayHashKey', 'gAttDrD6Q0COwHVG7YcXf47wRMUlfgCw');
    $HashIV = variable_get('ezpayHashIV', 'Ckbk8Ik6POLqtjnP');

    //交易資訊
    $remote_id = date('YmdHis') . $order->order_id;
    $Amount = $order->commerce_order_total['und'][0]['amount'];
    $price = commerce_currency_amount_to_decimal($Amount, 'TWD');
    $mail = $order->mail;
    $ItemDesc = variable_get('ezpayItemDesc', '使用HelloSanta模組購買商品');

    //時間限制
    $TradeLimit = isset($payment_method['settings']['TradeLimit']) ? $payment_method['settings']['TradeLimit'] : 60; //不適用在非即時

    //CrossMobile手機交易
    $CrossMobile = isset($payment_method['settings']['CrossMobile']) ? 0 : $payment_method['settings']['CrossMobile'];

    //傳送Data
    $data = array(
        'MerchantID' => variable_get('ezpayMerchantID', 'PG300000005853'),
        'TradeInfo' => '',
        'TradeSha' => '',
        'Version' => '1.0',
        'TimeStamp' => REQUEST_TIME,
        'MerchantOrderNo' => $remote_id,
        'Amt' => $price,
        'ItemDesc' => $ItemDesc,
        'TradeLimit' => $TradeLimit,
        'ClientBackURL' => $base_url . '/checkout/' . $order->order_id,
        'Email' => $mail,
        'CrossMobile' => $CrossMobile, //1 代表Wap交易 0代表Web交易
    );

    //加密資料
    $encrydata = ezpay_transaction_encrypt($data);
    $TradeInfo = $encrydata['TradeInfo'];
    $TradeSha = $encrydata['TradeSha'];

    $data['TradeInfo'] = $TradeInfo;
    $data['TradeSha'] = $TradeSha;

    foreach ($data as $name => $value) {
        if (!empty($value)) {
            $form[$name] = array('#type' => 'hidden', '#value' => $value);
        }
    }

    $form['#action'] = $ActionURL;
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Go'),
    );

    //建立交易記錄
    commerce_ezpay_create_payment_transactions($remote_id, $order, $payment_method, COMMERCE_PAYMENT_STATUS_PENDING);

    return $form;
}
